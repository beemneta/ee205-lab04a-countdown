///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Beemnet Alemayehu <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


int main(int argc, char* argv[]) {
   
   long seconds_now;
   long yr;
   long dy;
   long hr;
   long min;
   long sec;
   struct tm referT = {
      .tm_year=100,
      .tm_mon = 0,
      .tm_mday =0
   };

   time_t seconds_ref = mktime(&referT);
   long ex = 60; 
   long md = 60*60*24*365;
   long c = 60*60*24;
   long v = 60*60;

   char label[50];
   strftime(label, 50,"%a %b %d %r %Z %Y",&referT );
   printf("Reference time:  %s\n\n", label);
   
   while(1){
      seconds_now = time(NULL);
      long int diff = difftime(seconds_now,seconds_ref);

      yr = diff / md ;
      diff = diff - yr*md ;
      dy = diff / c;
      diff = diff - dy*c ;
      hr = diff / v;
      diff = diff - hr*v;
      min = diff /ex ;
      diff = diff - min*ex;
      sec = diff;   
      printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds:%ld \n",yr,dy,hr,min,sec);
      sleep(1);
   }


   return 0;
}
